package com.Onjt.TreatmentSelector;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TreatmentSelectorApplication {

	public static void main(String[] args) {
		SpringApplication.run(TreatmentSelectorApplication.class, args);
	}

}
