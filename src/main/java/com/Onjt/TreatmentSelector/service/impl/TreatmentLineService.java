package com.Onjt.TreatmentSelector.service.impl;

import com.Onjt.TreatmentSelector.entity.Treatment;
import com.Onjt.TreatmentSelector.entity.TreatmentLine;
import com.Onjt.TreatmentSelector.repository.TreatmentLineRepository;
import com.Onjt.TreatmentSelector.service.IService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
@Service
@Transactional
public class TreatmentLineService implements IService<TreatmentLine> {
    @Autowired
    private TreatmentLineRepository treatmentLineRepository;
    @Override
    public List<TreatmentLine> findAll() {
        return treatmentLineRepository.findAll();
    }

    @Override
    public TreatmentLine findById(Integer id) {
        return treatmentLineRepository.findById(id).get();
    }

    @Override
    public TreatmentLine save(TreatmentLine obj) {
        treatmentLineRepository.save(obj);
        return obj;
    }

    @Override
    public void deleteById(Integer id) {
        treatmentLineRepository.deleteById(id);
    }

    public void update(TreatmentLine updatedTreatmentLine){
        TreatmentLine existingTreatmentLine = treatmentLineRepository.findById(updatedTreatmentLine.getId()).get();
        existingTreatmentLine.setState(updatedTreatmentLine.getState());
        treatmentLineRepository.save(existingTreatmentLine);
    }
}
