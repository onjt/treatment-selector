package com.Onjt.TreatmentSelector.service.impl;

import com.Onjt.TreatmentSelector.entity.Category;
import com.Onjt.TreatmentSelector.repository.CategoryRepository;
import com.Onjt.TreatmentSelector.service.IService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
@Service
@Transactional
public class CategoryService implements IService<Category> {
    @Autowired
    private CategoryRepository categoryRepository;
    @Override
    public List<Category> findAll() {
        return categoryRepository.findAll();
    }

    @Override
    public Category findById(Integer id) {
        return categoryRepository.findById(id).get();
    }

    @Override
    public Category save(Category obj) {
        categoryRepository.save(obj);
        return obj;
    }

    @Override
    public void deleteById(Integer id) {
        categoryRepository.deleteById(id);
    }
}
