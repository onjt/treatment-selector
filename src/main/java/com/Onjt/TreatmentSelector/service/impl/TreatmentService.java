package com.Onjt.TreatmentSelector.service.impl;

import com.Onjt.TreatmentSelector.entity.Treatment;
import com.Onjt.TreatmentSelector.repository.TreatmentRepository;
import com.Onjt.TreatmentSelector.service.IService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
@Service
@Transactional
public class TreatmentService implements IService<Treatment> {
    @Autowired
    private TreatmentRepository treatmentRepository;
    @Override
    public List<Treatment> findAll() {
        return treatmentRepository.findAll();
    }
    @Override
    public Treatment findById(Integer id) {
        return treatmentRepository.findById(id).get();
    }
    @Override
    public Treatment save(Treatment obj) {
        treatmentRepository.save(obj);
        return obj;
    }
    @Override
    public void deleteById(Integer id) {
        treatmentRepository.deleteById(id);
    }

    public void update(Treatment updatedTreatment){
        Treatment existingTreatment = treatmentRepository.findById(updatedTreatment.getId()).get();
        existingTreatment.setName(updatedTreatment.getName());
        existingTreatment.setTraduction(updatedTreatment.getTraduction());
        existingTreatment.setState(updatedTreatment.getState());

        treatmentRepository.save(existingTreatment);
    }

}
