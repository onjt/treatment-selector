package com.Onjt.TreatmentSelector.service;

import java.util.List;

public interface IService <E>{
    public List<E> findAll();
    public E findById(Integer id);
    public E save(E obj);
    public void deleteById(Integer id);
}
