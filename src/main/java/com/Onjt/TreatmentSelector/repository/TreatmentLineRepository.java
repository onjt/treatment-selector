package com.Onjt.TreatmentSelector.repository;

import com.Onjt.TreatmentSelector.entity.TreatmentLine;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TreatmentLineRepository extends JpaRepository<TreatmentLine,Integer> {
}
