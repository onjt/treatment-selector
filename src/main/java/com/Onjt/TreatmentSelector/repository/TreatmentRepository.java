package com.Onjt.TreatmentSelector.repository;

import com.Onjt.TreatmentSelector.entity.Treatment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TreatmentRepository extends JpaRepository<Treatment,Integer> {
}
