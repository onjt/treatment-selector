package com.Onjt.TreatmentSelector.repository;

import com.Onjt.TreatmentSelector.entity.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryRepository extends JpaRepository<Category,Integer> {
}
