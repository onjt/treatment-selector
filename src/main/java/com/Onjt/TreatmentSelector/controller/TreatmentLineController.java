package com.Onjt.TreatmentSelector.controller;

import com.Onjt.TreatmentSelector.entity.Treatment;
import com.Onjt.TreatmentSelector.entity.TreatmentLine;
import com.Onjt.TreatmentSelector.service.impl.TreatmentLineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("treatment-line")
@CrossOrigin("http://localhost:5173")
public class TreatmentLineController {
    @Autowired
    private TreatmentLineService treatmentLineService;
    @GetMapping("/list")
    public List<TreatmentLine> findAll(){
        return treatmentLineService.findAll();
    }
    @GetMapping("/list/{id}")
    public TreatmentLine findById(@PathVariable("id") Integer id){
        return treatmentLineService.findById(id);
    }
    @PostMapping("/save/{id}")
    public TreatmentLine save(@RequestBody TreatmentLine treatmentLine, @PathVariable("id") Integer id){
        Treatment treatment = new Treatment();
        treatment.setId(id);
        treatmentLine.setTreatment(treatment);
        return treatmentLineService.save(treatmentLine);
    }
    @PutMapping("/update/{id}")
    public void update(@RequestBody TreatmentLine updatedTreatmentLine, @PathVariable("id") Integer id) {
        updatedTreatmentLine.setId(id);
        treatmentLineService.update(updatedTreatmentLine);
    }
}
