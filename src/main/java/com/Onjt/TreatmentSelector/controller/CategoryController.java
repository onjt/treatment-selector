package com.Onjt.TreatmentSelector.controller;

import com.Onjt.TreatmentSelector.entity.Category;
import com.Onjt.TreatmentSelector.service.impl.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/category")
@CrossOrigin("http://localhost:5173")
public class CategoryController {
    @Autowired
    private  CategoryService categoryService;

    @GetMapping("/list")
    public List<Category> findAll() {
        return categoryService.findAll();
    }
    @GetMapping("/list/{id}")
    public Category findById(@PathVariable("id") Integer id) {
        return categoryService.findById(id);
    }
    @PostMapping("/save")
    public Category save(@RequestBody Category category) {
        return categoryService.save(category);
    }
    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable("id") Integer id) {
        categoryService.deleteById(id);
    }

}
