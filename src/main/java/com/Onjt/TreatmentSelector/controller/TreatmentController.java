package com.Onjt.TreatmentSelector.controller;

import com.Onjt.TreatmentSelector.entity.Category;
import com.Onjt.TreatmentSelector.entity.Treatment;
import com.Onjt.TreatmentSelector.service.impl.TreatmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/treatment")
@CrossOrigin("http://localhost:5173")
public class TreatmentController {
    @Autowired
    private TreatmentService treatmentService;

    @GetMapping("/list")
    public List<Treatment> findAll() {
        return treatmentService.findAll();
    }
    @GetMapping("/list/{id}")
    public Treatment findById(@PathVariable("id") Integer id) {
        return treatmentService.findById(id);
    }
    @PostMapping("/save/{id}")
    public Treatment save(@RequestBody Treatment treatment, @PathVariable("id") Integer id) {
        Category category = new Category();
        category.setId(id);
        treatment.setCategory(category);
        return treatmentService.save(treatment);
    }
    @PutMapping("/update/{id}")
    public void update(@RequestBody Treatment updatedTreatment, @PathVariable("id") Integer id){
        updatedTreatment.setId(id);
        treatmentService.update(updatedTreatment);
    }
}
