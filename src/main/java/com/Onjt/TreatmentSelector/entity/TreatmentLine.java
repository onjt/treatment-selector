package com.Onjt.TreatmentSelector.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "treatment_line")
public class TreatmentLine implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_treatment_line", nullable = false)
    private Integer id;
    @Column(name = "date")
    private String date;
    @Column(name = "patient_name", length = 50)
    private String patientName;
    @Column(name = "state", length = 15)
    private String state;

    @ManyToOne(cascade = CascadeType.MERGE)
    private Treatment treatment;
}
