package com.Onjt.TreatmentSelector.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "treatment")
public class Treatment implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_treatment", nullable = false)
    private Integer id;
    @Column(name = "name", length = 50)
    private String name;
    @Column(name = "traduction", length = 50)
    private String traduction;
    @Column(name = "state")
    private Boolean state;

    @JsonIgnore
    @ManyToOne(cascade = CascadeType.MERGE)
    private Category category;

    @JsonIgnore
    @OneToMany(
            mappedBy = "treatment",
            cascade = CascadeType.MERGE,
            orphanRemoval = true
    )
    private List<TreatmentLine> treatmentLines = new ArrayList<>();
}
