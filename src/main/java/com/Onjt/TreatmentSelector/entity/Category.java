package com.Onjt.TreatmentSelector.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "category")
public class Category implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_category", nullable = false)
    private Integer id;
    @Column(name = "title", length = 50)
    private String title;

    @OneToMany(
            mappedBy = "category",
            cascade = CascadeType.MERGE,
            orphanRemoval = true
    )
    private List<Treatment> treatments = new ArrayList<>();
}
